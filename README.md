Feed2omb config to forward various rss feeds to GNU Social
==========================================================

To use this config we need [feed2omb](https://gitlab.com/CiaranG/feed2omb).
Then we place the config files from this repository in the
````feed2omb/config```` directory and add the passwords for the GNU Social
accounts.

The config can be tested with the following command:

````
python2 <path_to_feed2omb>/feed2omb.py --update --test <path_to_feed2omb>/feed2omb/config/<config_file>
````

If everything works fine we can create cronjobs to run the program
regularly. Something like:

````
0 * * * * python /home/schiesbn/bin/feed2omb/feed2omb.py --update  /home/schiesbn/bin/feed2omb/config/fsfe.news.config
30 * * * * python /home/schiesbn/bin/feed2omb/feed2omb.py --update  /home/schiesbn/bin/feed2omb/config/fsfe.events.config
15 * * * * python /home/schiesbn/bin/feed2omb/feed2omb.py --update  /home/schiesbn/bin/feed2omb/config/fsfe.twitter.config
45 * * * * python /home/schiesbn/bin/feed2omb/feed2omb.py --update  /home/schiesbn/bin/feed2omb/config/owncloud.twitter.config
10 * * * * python /home/schiesbn/bin/feed2omb/feed2omb.py --update  /home/schiesbn/bin/feed2omb/config/owncloud.news.config
````
